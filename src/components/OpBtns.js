import React from 'react'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAngleLeft, faAngleRight, faSyncAlt} from "@fortawesome/free-solid-svg-icons";

const OpBtns = (props) => {
    const loadMore = () => {
        props.page += 1
        props.loadMoreCity(false)
    }

    const scrollLeft = () => {
        let eleConBot = document.querySelector('.container-bot')
        eleConBot.scrollLeft -= 1000
    }

    const scrollRight = () => {
        let eleConBot = document.querySelector('.container-bot')
        eleConBot.scrollLeft += 1000
    }

    return(
        <div className="btn-ops">
            <button onClick={scrollLeft}>
                <FontAwesomeIcon icon={faAngleLeft}/>
            </button>
            <button onClick={scrollRight}>
                <FontAwesomeIcon icon={faAngleRight}/>
            </button>
            <button onClick={loadMore}>
                <FontAwesomeIcon icon={faSyncAlt}/>
            </button>
        </div>
    )
}

export default OpBtns
