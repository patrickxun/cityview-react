import React from 'react'

const OtherImage = (props) => {
    const title = props.title
    const rd = props.rd

    const clearSelected = () => {
        document.querySelectorAll('.imgCard-content').forEach(ele => ele.classList.remove('selected'))
    }

    return (
        <div className="container-bot">
            <div className="other-img-wrap">
                {props.otherImg.map((img, index) =>
                    <div className="imgCard" key={img.id} onClick={(e) => {
                        clearSelected()
                        e.target.classList.add('selected')
                        props.setContainerOp(0)
                        props.changeBg(img.urls.regular, img.color, title)
                        props.changeStorage(img.urls.regular, img.color, title)
                    }}>
                        <img className={index === rd ? "imgCard-content selected" : "imgCard-content"}
                             src={img.urls.thumb} alt=""/>
                    </div>
                )
                }
            </div>
        </div>
    )
}

export default OtherImage
