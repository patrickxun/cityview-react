import './App.css';
import React, {useState, useEffect} from 'react'
import {getRandom, hexToRgb} from "./Functions";
import Title from './components/Title'
import OtherImage from "./components/OtherImg";
import OpBtns from "./components/OpBtns";
import InputSearch from "./components/InputSearch";

const api_pre = 'https://api.unsplash.com/search/photos?query='
const api_sub = '&client_id='
const accessKey = '-5xjwbxb2JzQOmLYwgMnGKua3hOk_Wp2thETHJTlO7o'
const keyName = 'cityList'
let rd = 0
let page = 1

function App() {
    const [city, setCity] = useState('')
    const [title, setTitle] = useState('')
    const [imgUrl, setImgUrl] = useState('')
    const [color, setColor] = useState([])
    const [containerOp, setContainerOp] = useState(0)
    const [isLoading, setIsLoading] = useState(false)
    const [otherImg, setOtherImg] = useState([])


    useEffect(() => {
        loadStorage()
    }, [])


    const changeBg = (url, color, city) => {
        let colorHex = color.replace('#', '')
        let colorRgb = hexToRgb(colorHex)
        setColor([colorRgb[0], colorRgb[1], colorRgb[2]])

        setTimeout(() => {
            setImgUrl(url)
            setTimeout(() => {
                setTitle(city.trim().toUpperCase())
                setContainerOp(1)
            }, 1000)
        }, 800)
    }

    const errorBg = () => {
        setTimeout(() => {
            setImgUrl("https://images.unsplash.com/photo-1623018035782-b269248df916?crop=entropy&cs=srgb&fm=jpg&ixid=MnwyNDQ1NjN8MHwxfHNlYXJjaHw2fHxFUlJPUnxlbnwwfHx8fDE2MjU2MjUyODc&ixlib=rb-1.2.1&q=85")
            setTimeout(() => {
                setCity('')
                setContainerOp(1)
            }, 500)
        }, 800)
    }

    const changeStorage = (url, color, city) => {
        let cityStorages = JSON.parse(window.localStorage.getItem(keyName))
        cityStorages.push({
            url: url,
            city: city,
            color: color,
        })
        window.localStorage.setItem(keyName, JSON.stringify(cityStorages))
    }

    const loadStorage = () => {
        let cities = JSON.parse(window.localStorage.getItem(keyName))
        if (!cities) {
            window.localStorage.setItem(keyName, JSON.stringify([{
                    city: 'guangzhou',
                    url: api_pre + 'guangzhou' + api_sub + accessKey,
                    color: 'black'
                }])
            )
            changeBg('https://www.arch2o.com/wp-content/uploads/2015/08/Arch2O-case-study-the-parametric-twist-of-canton-tower-2.png', 'black', 'GUANGZHOU')
            return
        }
        if (cities.length === 1) {
            changeBg('https://www.arch2o.com/wp-content/uploads/2015/08/Arch2O-case-study-the-parametric-twist-of-canton-tower-2.png', 'black', 'GUANGZHOU')
            return
        }
        if (cities.length > 1) {
            let lastData = cities[cities.length - 1]
            let lastDataUrl = lastData.url
            let lastDataCity = lastData.city
            let lastDataColor = lastData.color
            changeBg(lastDataUrl, lastDataColor, lastDataCity)
        }
    }

    const loadNewCity = () => {
        page = 1
        setOtherImg([])

        let api = api_pre + city.trim().toLowerCase() + api_sub + accessKey + '&page=' + page

        setContainerOp(0)
        setIsLoading(true)

        fetch(api)
            .then(response => response.json())
            .then(data => {
                setTitle('')

                rd = getRandom(data.results.length)
                let result = data.results[rd]

                let imgUrl = result.urls.regular
                let imgColor = result.color

                changeBg(imgUrl, imgColor, city)
                changeStorage(imgUrl, imgColor, city)

            })
            .catch(() => errorBg())
            .finally(() => {
                setIsLoading(false)
                setCity('')
            })
    }

    const loadMoreCity = (auto = true) => {
        let api
        if (auto) {
            api = api_pre + city.trim().toLowerCase() + api_sub + accessKey + '&page=' + page
        } else {
            api = api_pre + title.trim().toLowerCase() + api_sub + accessKey + '&page=' + page
        }
        fetch(api)
            .then(response => response.json())
            .then(data => {
                setOtherImg(otherImg => [...otherImg, ...data.results])

                if (page !== 1) {
                    let eleConBot = document.querySelector('.container-bot')
                    setTimeout(() => {
                        eleConBot.scrollLeft += 100000
                    }, 1000)
                }

            })
            .catch((err) => console.error(err))
    }




    return (
        <div className="App" style={{backgroundColor: `rgb(${color[0]}, ${color[1]}, ${color[2]})`}}>
            <div className="container"
                 style={{backgroundImage: `url('${imgUrl}')`, opacity: containerOp}}>
                <Title title={title} color={color} containerOp={containerOp}/>
                <OtherImage otherImg={otherImg} title={title}
                            setContainerOp={setContainerOp} changeBg={changeBg} changeStorage={changeStorage} rd={rd}/>
                {otherImg.length !== 0 &&
                <OpBtns page={page} loadMoreCity={loadMoreCity}/>
                }
            </div>
            <InputSearch city={city} loadNewCity={loadNewCity} loadMoreCity={loadMoreCity} setCity={setCity}/>
            {isLoading && <div className="loading"></div>}
        </div>
    )
}

export default App;
