import React from 'react'

const Title = (props) =>{

    return(
        <div id="title" className="title"
             style={{color: `rgb(${props.color[0]}, ${props.color[1]}, ${props.color[2]})`, opacity: props.containerOp}}>
            {props.title}
        </div>
    )
}

export default Title
