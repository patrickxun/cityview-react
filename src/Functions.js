export const getRandom = (range) => {
    return Math.floor(Math.random() * range)
}

export const hexToRgb = (hex) => {
    let bigint = parseInt(hex, 16);
    let r = (bigint >> 16) & 255;
    let g = (bigint >> 8) & 255;
    let b = bigint & 255;

    return [255 - r, 255 - g, 255 - b];
}
