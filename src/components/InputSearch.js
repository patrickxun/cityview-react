import React from 'react'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faGoogle} from "@fortawesome/free-brands-svg-icons";

const InputSearch = (props) => {
    const city = props.city

    const handleCityChange = (e) => {
        props.setCity(e.target.value)
    }

    const enterLoadNewCity = (e) => {
        if (e.key === 'Enter') {
            props.loadNewCity()
            props.loadMoreCity()
        }
    }

    return(
        <>
            <input id='cityName' type="text" placeholder="City..." value={city} autoComplete="off" autoFocus
                   onChange={handleCityChange}
                   onKeyDown={(e) => {
                       enterLoadNewCity(e)
                   }}/>
            <button onClick={() => {
                props.loadNewCity()
                props.loadMoreCity()
            }}>
                <FontAwesomeIcon icon={faGoogle}/>
            </button>
        </>
    )
}

export default InputSearch
